## Formal Verification for Feature-based Composition of Workflows

All source code can be view with a *Browser* via the following link: [LoadAllSERENE18.html](http://www.cs.swan.ac.uk/~csetzer/transfer/serene18Temporary8jmwU4hB3ny/GUI.LoadAllSERENE18.html)

![Screencast Monadic Example](./ScreencastMonadic.gif)


Files Used in the article submitted to SERENE'18 by S. Adelsberger, B. Igried, M. Moser, V. Savenkov, and A. Setzer

##### Agda Code in Emacs:

2. Agda Code
  [agda](/agda/)
3. Start file loading all files of the paper by chapter:
  [LoadAllSERENE18.agda](/agda/examples/GUI/LoadAllSERENE18.agda)

Refer to ['README_INSTALL.txt'](/agda/misc/README_INSTALL.txt) for installation instructions.
